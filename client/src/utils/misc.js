export function dateFormatter(dateString) {
  const actualDate = new Date()
  const fecha = new Date(Date.parse(dateString))
  const yesterday = new Date(
    actualDate.getFullYear(),
    actualDate.getMonth(),
    actualDate.getDate() - 1,
    23,
    59,
    59,
  )
  if (fecha.getTime() >= yesterday.getTime()) {
    return `${
      (fecha.getHours() + 1) % 12 === 0 ? 12 : (fecha.getHours() + 1) % 12
    }:${
      fecha.getMinutes() < 10 ? `0${fecha.getMinutes()}` : fecha.getMinutes()
    } ${fecha.getHours() < 11 ? 'am' : 'pm'}`
  }
  const newYesterday = new Date(yesterday.getTime() - millisecondsInDay)
  if (
    fecha.getTime() < yesterday.getTime() &&
    fecha.getTime() > newYesterday.getTime()
  ) {
    return 'Yesterday'
  }
  return `${months[fecha.getMonth()]} ${fecha.getDate()}`
}
const millisecondsInDay = 86400000
const months = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
]
