import axios from 'axios'

const API = {}
const baseURL = `http://localhost:8080/api`

const responseHandler = (response) => {
  const { data, status, statusText } = response
  if (status === 200) {
    return data
  } else {
    return Promise.reject(statusText)
  }
}
const axiosInstance = axios.create({
  baseURL,
  validateStatus() {
    return true
  },
})
axiosInstance.interceptors.response.use(responseHandler, (error) =>
  Promise.reject(error),
)

API.get = (url) => axiosInstance.get(url)
API.delete = (url) => axiosInstance.delete(url)
export default API
