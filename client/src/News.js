import React, { useRef } from 'react'
import { Grid, Typography, makeStyles, IconButton } from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'
import { dateFormatter } from './utils/misc'

export default function News({ divider = false, data, onDelete }) {
  const classes = useStyles()
  const dateFormatted = useRef(dateFormatter(data.createdAt))
  return (
    <Grid
      container
      className={classes.rowContainer}
      justify="flex-start"
      alignItems="center"
    >
      <Grid
        item
        xs={9}
        md={9}
        container
        alignItems="center"
        style={{ cursor: `pointer` }}
        onClick={() => window.open(data.url, '_blank')}
      >
        <Typography align="left" className={classes.title}>
          {data.title}
        </Typography>
        <Typography align="left" className={classes.author}>
          {`- ${data.author} -`}
        </Typography>
      </Grid>

      <Grid container xs alignItems="center" justify="flex-end">
        <Grid item>
          <Typography align="center" className={classes.date}>
            {dateFormatted.current}
          </Typography>
        </Grid>
        <Grid item>
          <IconButton onClick={() => onDelete(data.id)}>
            <DeleteIcon />
          </IconButton>
        </Grid>
      </Grid>
    </Grid>
  )
}

const useStyles = makeStyles({
  rowContainer: {
    backgroundColor: `#fff`,
    borderBottom: `1px solid #ccc`,
    paddingLeft: `2rem`,
    margin: `0 5rem`,
    '&:hover': {
      backgroundColor: `#fafafa`,
    },
  },
  title: {
    display: `flex`,
    alignItems: `center`,
    height: `5rem`,
    color: `#333`,
    fontSize: `13pt`,
  },
  author: {
    color: `#999`,
    marginLeft: `5px`,
  },
  date: {
    paddingRight: `1rem`,
  },
})
