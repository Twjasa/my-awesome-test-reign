import { CircularProgress, Grid, makeStyles } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import './App.css'
import Header from './Header'
import News from './News'
import API from './utils/api'

function App() {
  const classes = useStyles()
  const [news, setNews] = useState(undefined)
  const [isLoading, setIsloading] = useState(true)

  const getNews = async () => {
    const data = await API.get(`/news`)
    setIsloading(false)
    setNews(data)
  }

  useEffect(() => {
    getNews()
  }, [])

  const onDelete = async (id) => {
    setIsloading(true)
    API.delete(`/news/${id}`)
      .then(() => {
        getNews()
      })
      .catch((e) => {
        console.log(e)
        setIsloading(false)
      })
  }

  return (
    <div className="App">
      <Header />
      {isLoading && (
        <Grid
          className={classes.body}
          container
          justify="center"
          alignContent="center"
        >
          <CircularProgress />
        </Grid>
      )}
      <Grid xs container className={classes.bodyNews}>
        {news &&
          !isLoading &&
          news.map((item, key) => {
            return <News data={item} onDelete={(id) => onDelete(id)} />
          })}
      </Grid>
    </div>
  )
}
const useStyles = makeStyles({
  bodyNews: {
    marginTop: `0.8rem`,
  },
  body: {
    height: `77vh`,
  },
})
export default App
