import React from 'react'
import { Grid, Typography, makeStyles } from '@material-ui/core'

export default function Header() {
  const classes = useStyles()
  return (
    <Grid
      className={classes.bannerHeader}
      justify="center"
      align="flex-start"
      container
      direction="column"
    >
      <Typography variant="h1" align="left" className={classes.titleHeader}>
        HN Feed
      </Typography>
      <Typography variant="h3" align="left" className={classes.subtitleHeader}>
        {`We <3 hacker news!`}
      </Typography>
    </Grid>
  )
}

const useStyles = makeStyles({
  bannerHeader: {
    backgroundColor: '#333',
    height: `23vh`,
    paddingLeft: `4rem`,
  },
  titleHeader: {
    fontWeight: '700',
    color: '#ffff',
    fontSize: '5rem',
  },
  subtitleHeader: {
    fontWeight: '700',
    color: '#ffff',
    paddingTop: '0.5rem',
    fontSize: '1.30rem',
  },
})
