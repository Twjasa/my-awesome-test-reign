require('dotenv').config()

const { HOST, DB, DB_PORT, API_HN, PORT } = process.env
module.exports = {
  HOST,
  DB,
  DB_PORT,
  API_HN,
  PORT,
}
