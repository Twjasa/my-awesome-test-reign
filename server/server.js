'use strict'

const express = require(`express`)
const connectDb = require('./src/database/connection')
const News = require('./src/model/News')
const cors = require('cors')
const cron = require('node-cron')
const {
  getNewsFromAPIHN,
  newsRouter,
} = require('./src/controllers/newsController')
const app = express()

// Constants
const PORT = 8080
const HOST = '0.0.0.0'

const inserManyFromApi = () => {
  getNewsFromAPIHN()
    .then(async (newsData) => {
      await News.insertMany(newsData, {
        ordered: false,
      })
      console.log(`All news has been updated`)
    })
    .catch((e) => {
      console.log(`Uh oh... something went wrong: ${e}`)
    })
}
const firstRun = async () => {
  const hasRecord = await News.find({})
  if (hasRecord.length === 0) {
    inserManyFromApi()
  }
}
firstRun()
//cron job every hour
cron.schedule('00 * * * *', () => {
  console.log('Please hold, our midgets are fetching all the news')
  inserManyFromApi()
})

app.use(cors())

//Routes
app.get('/', (req, res) => {
  res.send('Hello World')
})

//Controllers
app.use('/api/news', newsRouter)

app.listen(PORT, () => {
  console.log(`Running on http://${HOST}:${PORT}`)
  connectDb().then(() => {
    console.log('MongoDb connected')
  })
})
