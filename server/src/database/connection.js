const mongoose = require('mongoose')
const News = require('../model/News')

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
}
const connection = 'mongodb://mongo:27017/mongo-db'

const connectDb = () => {
  return mongoose.connect(connection, options)
}
module.exports = connectDb
