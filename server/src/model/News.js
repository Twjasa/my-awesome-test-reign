const mongoose = require('mongoose')

const newsSchema = new mongoose.Schema({
  id: {
    type: String,
    required: true,
    unique: true,
  },
  author: {
    type: String,
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  url: {
    type: String,
    required: true,
  },
  erased: {
    type: Boolean,
    required: true,
  },
  createdAt: {
    type: Date,
    required: true,
  },
})

const News = mongoose.model('News', newsSchema)

module.exports = News
