const axios = require('axios')
const newsRouter = require('express').Router()
const config = require('../../config/config')
const News = require('../model/News')

const getNewsFromAPIHN = async () => {
  const response = await axios.get(config.API_HN)
  const newsArray = response.data.hits
    .reduce((accumulator, currentValue) => {
      let {
        story_title: storyTitle,
        title,
        url,
        story_url: storyUrl,
        author,
        story_id: id,
        created_at: createdAt,
      } = currentValue
      title = title || storyTitle
      url = url || storyUrl
      return [
        ...accumulator,
        { title, url, author, id, createdAt, erased: false },
      ]
    }, [])
    .sort((a, b) => {
      return new Date(b.createdAt) - new Date(a.createdAt)
    })
  const seen = new Set()
  const uniqueNews = newsArray.filter((item) => {
    const duplicate = seen.has(item.id)
    seen.add(item.id)
    return !duplicate
  })
  return uniqueNews
}
newsRouter.delete('/:id', async (request, response) => {
  await News.findOne({ id: request.params.id }, (error, news) => {
    if (!error && news) {
      news.erased = true
      news.save().then(() => console.log(`News erased`))
    }
  })
  response.send('News deleted \n')
})

newsRouter.get('/', (request, response) => {
  console.log(`Getting info...`)
  News.find({ erased: false }, (err, news) => {
    if (err) {
      console.log(`Error: ${err}`)
    }
    response.json(news)
    console.log(`Done!`)
  }).catch((e) => {
    console.log(e)
  })
})

module.exports = {
  getNewsFromAPIHN,
  newsRouter,
}
